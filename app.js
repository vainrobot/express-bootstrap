var secure_keys,
	express = require("express"),
	https = require("https"),
	app = express(secure_keys),
	redis = require("redis"),
	RedisStore = require("connect-redis")(express),
	swe = {},
	helper = require("./libs/HELPER.js"),
	mw = require("./libs/MW.js");

/*--------------requires----------*/

////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////

/*--------------config------------*/

app.set("views", __dirname + "/views");
app.set("view engine", "jade");

app.use(express.favicon(__dirname + '/public/favicon.ico'));//, { maxAge: 2592000000 })); 


app.configure("development", function(){
	app.use(express.logger("\x1b[33m:method\x1b[0m \x1b[32m:url\x1b[0m :response-time"));
	app.use(express.methodOverride());
	app.use(express.bodyParser());
	app.use(express.static(__dirname + "/public"));
	app.use(express.cookieParser("expressstuff")); //@TODO
    app.use(express.session({
		store: new RedisStore({
			host: "localhost",
			port: 6379,
			db: 16, //@TODO change this!! 
			ttl: 90 //so this times out really fast easier for me to catch interruption bugs
		}), 
		secret: "the_secret_sauce"
	}));
    app.use(express.errorHandler({
		dumpExceptions: true, 
		showStack: true 
	}));
	app.use(app.router);
	//SWE vars would be pulled here // facebook creds etc
	//swe = process.env.mytodo || "@TODO";
});

app.configure("production", function(){
	var oneYear = 31557600000;
		app.use(express.static(__dirname + "/public", { 
		maxAge: oneYear 
	}));
	app.use(express.errorHandler());	
});


/*------------dynamic helpers replaced with locals in express 3.x---------------*/

var port = process.env.PORT || 8000;
//check development / staging / production not typo'd
app.listen(port);
console.log("Express started with Jade on port " + port);



////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////
////////////////////////////////////////////////////

/*-------------routes-------------------------------------------------------------*/

app.get("/secret", mw.hasSessionAbility, function(req,res){
	res.render("private_home",{
		_page: "private_home",
		_message: "Welcome to the ES bootstrap private secret page @TODO",
		_link: ""
	});
});

app.get("/", function(req,res){
	res.render("public_home",{
		_page: "public_home",
		_message: "Welcome to the ES public homepage",
		_link: ""
	});
});
/*-------------MAIN API JSON----------------------------------------------------------------*/
//mw.enforceXHR
app.get("/json", function(req,res){
	res.json({"json_test":"just testing the main API"});
});