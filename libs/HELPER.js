/**
 * HELPER
 * 
 * @TODO Module description
 * @return {Function} _releasePublicObjects
 */
var HELPER = (function(){
	/*****************************************
	******************************************
	**************** PRIVATE *****************
	******************************************
	*****************************************/
	/**
	 * [Private]
	 * _Release Public Objects
	 *
	 * Exposes only the public objects.
	 *
	 * @return {Function} init
	 */
	function _releasePublicObjects(){
		return {
			getUniqueString: getUniqueString,
			makeSafe: makeSafe
		}
	};

	/*****************************************
	******************************************
	**************** PUBLIC ******************
	******************************************
	*****************************************/

	/**
	 * Get Unique String
	 *
	 * Creates a unique number
	 *
	 * @return {String}
	 */
	function getUniqueString(){
		return new Date(); //for now
	};

	/** @TODO
	 * Make Safe
	 *
	 * Stops garbage hitting the server
	 *
	 * @param {String} //anything
	 * @return passed or null
	 */
	function makeSafe(unsafe){
		var safe = null,
			pattern = /d{3}/;
		//redo all this properly
		if(unsafe === "facebook"){
			safe = "facebook";
		}
		return safe;
	};


	return _releasePublicObjects();
})();

module.exports = HELPER;

