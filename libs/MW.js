/**
 * MW
 * 
 * @TODO Module description
 * @return {Function} _releasePublicObjects
 */
var MW = (function(){
	/*****************************************
	******************************************
	**************** PRIVATE *****************
	******************************************
	*****************************************/
	/**
	 * [Private]
	 * _Release Public Objects
	 *
	 * Exposes only the public objects.
	 *
	 * @return {Function} init
	 */
	function _releasePublicObjects(){
		return {
			hasSessionAbility: hasSessionAbility
		}
	};

	/*****************************************
	******************************************
	**************** PUBLIC ******************
	******************************************
	*****************************************/

	function hasSessionAbility(req,res,next){
		if(req.session){
			console.log("WOOT we have a req.session ");
			/*
				This session may have just expired and a new one reissued?
				What happens when we turn cookies off?
			

				next steps check for an es_id
			*/
		
			if(req.session.es_id){
				//golden they are logged in

			}else{
				console.log("WOOT we have a req.session but no es_id, you must not be logged in");
				res.redirect("/");
				return;
			}
			
			/*

			*/


			console.log(req.session);
			next();
		}else{
			console.log("We have a redis session problem");
				res.render("error",{
					_page: "error",
					_message: "Failed to log in " 
								+ " "
								+ ", because " 
								+ "we have a redis session problem"
								+ "",
					_link: "/"
				});
		}
	};

	return _releasePublicObjects();
})();

module.exports = MW;