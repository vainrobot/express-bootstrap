EB = (function(){
	function _release_public(){
		return {
			init: init,
			loginChoice: loginChoice,
			redirectTo: redirectTo
		};
	}
	
	

	/*-------public--------*/
	
	function init(){
		//remove all these fucking comments, use zombie for debugging
		console.log(this.name + "::Init: This write was " +
				"called by JS as an instance of EB named " + this.name);
		$(".dialog").find("h1").html("@TODO loaded").end().addClass("hidden");
	}

	function loginChoice(){
		//should remove the login button or fade out rest etc?
		console.info("Handler EH > EBx" + "::loginChoice: Load up a dialog box with login options");
		var html = '<input required placeholder="some login stuff"></input>';
		$(".dialog").find("h1").html("Please login").end().removeClass("hidden").find("p").html(html);
	}

	/**
	 * Redirect To
	 * 
	 * param {String} relative url //login-with/facebook
	 * 
	 */
	function redirectTo(url){
		console.info(this.name + "::redirectTo: Going to " + url);
		location.href = url;
	}

	
	return _release_public();
})();


GEB.EB1 = Object.create(EB, {
	name: {
		value: "EB1"
	},
	init: {
		value: EB.init
	},
	loginChoice: {
		value: EB.loginChoice
	},
	redirectTo: {
		value: EB.redirectTo
	}
});

delete EB;

GEB.EB1.init();