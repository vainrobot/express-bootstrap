HP = (function(){
	function _release_public(){
		return {
			init: init
		};
	};
	
	/*-------public--------*/
	
	function init(){
		//remove these comments
		console.log(this.name + "::Init: This write was " +
				"called by JS as an instance of HP named " + this.name);
	};
	
	return _release_public();
})();

GEB.HP1 = Object.create(HP, {
	name: {
		value: "HP1"
	},
	init: {
		value: HP.init
	}
});

delete HP;

GEB.HP1.init();