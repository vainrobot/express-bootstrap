RS = (function(){
	function _release_public(){
		return {
			init: init,
			resize: resize
		};
	}
	
	/*-------public--------*/
	
	function init(){
		console.log(this.name + "::Init: This write was " +
				"called by JS as an instance of RS named " + this.name);
	}
	
	/**
	 * Resize
	 * 
	 * Once you resize a window we need to check for a breakpoint
	 * and set a class on the body.
	 *
	 * param{Number} widthN
	 *
	 */
	function resize(widthN){
		//console.info(this.name + "::Resizer: Width passed: " + widthN);
		var size_class_str = "";
		if(widthN < 481){
			size_class_str = "mobile";
		}else if(widthN < 801){
			size_class_str = "tablet";
		}else{
			//console.log(this.name + "::Resize: stay on desktop size:" + widthN);
		}
		$("body").removeClass().addClass(size_class_str);
	}

	return _release_public();
})();

GEB.RS1 = Object.create(RS, {
	name: {
		value: "RS1"
	},
	init: {
		value: RS.init
	},
	resize: {
		value: RS.resize
	}
});

delete RS;

GEB.RS1.init();