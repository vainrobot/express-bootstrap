/**
  * ZOMBIE to test EB.js
  *
  */

var Browser = require("zombie"),
	assert = require("assert"),
	nock = require("nock");

browser = new Browser({
	silent: true
});

/** @TODO
 * Ensure EB.init() is called on load
 * not sure how to do this yet
 * 
 */
describe("Home page", function(){
	it("Contains a wrapper class", function(done){
		browser.visit("http://localhost:8000/", function(){
			assert.ok(browser.success);
			assert.ok(browser.query(".wrapper"));
			done();
		});
	});
});

/** 
 * Ensure mocks work
 * demo: how to use a mock
 * 
 */
describe("Google mock", function(){
	var scope = nock('http://www.google.com')
                .get('/')
                .reply(200, "Hello from mock Google!");
	it("Talks to google mock", function(done){
		browser.visit("http://www.google.com", function(){
			assert.equal(browser.text("body"), "Hello from mock Google!");
			done();
		});
	});
});


/** @WIP
 * Ensure EB.init() replace the dialog h1 text node with the 
 * words '@TODO EB loaded'.
 * 
 */
describe("Dialog check", function(){
	it("Talks of bootstrapped world", function(done){
		browser.visit("http://localhost:8000/", function(){
			assert.equal(browser.text(".dialog h1"), "@TODO loaded");
			done();
		});
	});
	it("Check if it is hidden to start with", function(done){
		browser.visit("http://localhost:8000/", function(){
			//@WIP 
			//This is a bug, and I also have this test in features/login.js
			//remove one
			assert.notEqual(browser.query(".dialog").getAttribute("class").indexOf("hidden"), -1);
			//is it really hidden CSS? mmmm?
			done();
		});
	});
});