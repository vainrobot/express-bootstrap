/**
  * ZOMBIE to test login features
  *
  */

var Browser = require("zombie"),
	assert = require("assert"),
	nock = require("nock");

browser = new Browser({
	silent: true
});



/** 
 * People must not see login dialog until they click it
 *
 * i.e dialog should be hidden
 * 
 */
 describe("People should not see login dialog box until they press login", function(){
	it("When they go to the homepage the dialog box should be hidden", function(done){
		browser.visit("http://localhost:8000/", function(){
			//there is a potential zombie bug here
			//@TODO FIX 
			//this will fail but it is actually ok
			assert.equal(browser.query(".dialog").className, "dialoghidden");

			//assert.equal(browser.query(".dialog").className, "dialog hidden");
		});
	});
});


/** 
 * People must be offered an option to login
 *
 * When they click the login button and see a dialog showing a login box
 * 
 */
describe("People must be offered an option to login", function(){
	it("When they click a login button they see the words login in a dialog box", function(done){
		browser.visit("http://localhost:8000/", function(){
			assert.equal(browser.text(".login_btn"), "login");
			browser.pressButton(".login_btn", function(){
				//this is a false +ve when EH or JQ does not load
				//please fix
				assert.equal(browser.query(".dialog").getAttribute("class").indexOf("hidden"), -1);
				assert.equal(browser.text(".dialog h1"), "Please login");
				done();
			});
		});
	});
	//@WIP
	/*
		A lot to do here
	*/

	//then they should be redirected
});





